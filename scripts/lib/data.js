(function () {
  var configDir = data.configDir;
  var profile = data.profile;
  Object.defineProperties(data, {
      /** 
       * The bookmark file
       * @name bookmarks 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "bookmarks" :  { value : configDir + "/" + profile + "/bookmarks", enumerable : true }, 
      /** 
       * The history file
       * @name history 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "history"   :  { value : configDir + "/" + profile + "/history", enumerable : true },
      /** 
       * The cookie file
       * @name cookies 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "cookies"   :  { value : configDir + "/" + profile + "/cookies", enumerable : true }, 
      /** 
       * The quickmarks file
       * @name quickmarks 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "quickmarks" : { value : configDir + "/" + profile + "/quickmarks", enumerable : true },
      /** 
       * The whitelist for persistent cookies
       * @name cookiesWhitelist 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "cookiesWhitelist"  :  { value : configDir + "/" + profile + "/cookies.allow", enumerable : true }, 
      /** 
       * The whitelist for session cookies
       * @name sessionCookiesWhitelist 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "sessionCookiesWhitelist"   :  { value : configDir + "/" + profile + "/cookies_session.allow", enumerable : true }, 
      /** 
       * The whitelist for plugins
       * @name pluginsWhitelist 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "pluginsWhitelist"  :  { value : configDir + "/" + profile + "/plugins.allow", enumerable : true }, 
      /** 
       * The whitelist for scripts
       * @name scriptWhitelist 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "scriptWhitelist"   :  { value : configDir + "/" + profile + "/scripts.allow", enumerable : true }, 
      /** 
       * The session file
       * @name session 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "session"   :  { value : configDir + "/" + profile + "/session", enumerable : true }, 
      /** 
       * The custom keys file
       * @name customKeys 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "customKeys"  :  { value : configDir + "/" + profile + "/custom_keys", enumerable : true }, 
      /** 
       * The keyboard configuration file
       * @name keys 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "keys"  :  { value : configDir + "/keys", enumerable : true }, 
      /** 
       * The settings configuration file
       * @name settings 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "settings"  :  { value : configDir + "/settings", enumerable : true }, 
      /** 
       * The searchengines file
       * @name searchengines 
       * @memberOf data
       * @readonly
       * @type String
       * */
      "searchEngines"   :  { value : configDir + "/searchengines", enumerable : true }, 
  });
})();
Object.freeze(data);
